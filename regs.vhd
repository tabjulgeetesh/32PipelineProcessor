library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity regFile is
	Port(
		clk : in std_logic ;
		regXAddress : in std_logic_vector (4 downto 0);
		regYAddress : in std_logic_vector (4 downto 0);
		writeAddress : in std_logic_vector (4 downto 0);
		regInputData : in std_logic_vector(31 downto 0) ;
		rin : in std_logic ;
		rout : in std_logic ;
		r_mem : in std_logic ;
		output_to_mem : out std_logic_vector(31 downto 0);
		outputRegX : out std_logic_vector(31 downto 0):="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ" ;
		outputRegY : out std_logic_vector(31 downto 0):="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
		reg_to_mem_address : in std_logic_vector(4 downto 0) 
	) ;
end regFile;

architecture Behavioral of regFile is
	type regs is array(9 downto 0) of std_logic_vector(31 downto 0) ;
	signal registers : regs  := (others => (others => '0')) ;
begin

	process (rin) begin
		--if rising_edge(clk) then
			if rin = '1' then
				registers(to_integer(unsigned(writeAddress))) <= regInputData ;
				--output <= registers(to_integer(unsigned(address))) ;
			end if;
	end process;
	process (rout) begin
			if rout = '1' then
				if (regXAddress /= "ZZZZZ") then
					outputRegX <= registers(to_integer(unsigned(regXAddress))) ;
				end if;
				if (regYAddress /= "ZZZZZ") then
					outputRegY <= registers(to_integer(unsigned(regYAddress))) ;
				end if;
			else
				outputRegY <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
			end if ;
	end process;
	process(r_mem) begin
			if r_mem = '1' then
				output_to_mem <= registers(to_integer(unsigned(reg_to_mem_address)));
			end if;
		--end if;
	end process ;
end Behavioral;

