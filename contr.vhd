library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fetch is
	Port(
		clk : in std_logic;
		start : in std_logic ;
		done : out std_logic ;
		pc : in std_logic_vector(7 downto 0) ;
		pcfetch : out std_logic_vector(7 downto 0) ;
		ram_address : out std_logic_vector(7 downto 0);
		buf1 : out std_logic_vector(31 downto 0) ;
		ram_data : in std_logic_vector(31 downto 0) ;
		ram_read : out std_logic
	) ;
end fetch;

architecture fetchbeh of fetch is
type states is (f1, f2, f3) ;
signal state : states := f1;
attribute keep : string;
attribute keep of state : signal is "true";
begin
	process (clk,state) 
	begin
		if rising_edge(clk) and (start = '1') then
			if (state = f1) then
				done <= '0';
				Report "its a sunday";
				ram_address <= pc;
				ram_read <= '1';
				state <= f2 ;
			end if;
		end if;
		if rising_edge(clk) then
			if (state = f3) then
				buf1 <= ram_data;
				ram_read <= 'Z';
				ram_address <= "ZZZZZZZZ";
				if (ram_data(31 downto 28) /= "1111") then
					pcfetch <= std_logic_vector(to_unsigned(to_integer(unsigned(pc))+1 , 8)) ;
				else
					pcfetch <= pc;
				end if;
				done <= '1';
				state <= f1;
			elsif (state = f2) then
				state <= f3 ;
			else 

			end if;
		end if;
	end process;
end fetchbeh;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity decode is
	Port(
		clk : in std_logic;
		start : in std_logic;
		done : out std_logic;
		buf1 : in std_logic_vector(31 downto 0);
		buf2 : out std_logic_vector(31 downto 0);
		reg_out : out std_logic ;
		reg_addressX : out std_logic_vector(4 downto 0);
		reg_Y : out std_logic_vector(31 downto 0);
		reg_addressY : out std_logic_vector(4 downto 0)
		) ;
end decode;

architecture decodebeh of decode is
type states is (d1,d2,d3);
signal state : states := d1;
signal check : std_logic := '0';
attribute keep : string;
attribute keep of state : signal is "true";
begin
	process (clk) begin
		if rising_edge(clk) then
			if (state = d1) and (start = '1') then
				
				if (buf1(31 downto 28) = "0000") or (buf1(31 downto 28) = "0001") or (buf1(31 downto 28) = "0100") or (buf1(31 downto 28) = "0101") or (buf1(31 downto 28) = "0110") then
					reg_addressY <= buf1(22 downto 18);
					reg_addressX <= buf1(17 downto 13);
					reg_out <= '0' ;
					state <= d2 ;
					done <= '0';
				elsif (buf1(31 downto 28) = "0011") or (buf1(31 downto 28) = "0010") then
					reg_addressX <=  buf1(4 downto 0) ;
					
					reg_out <= '0' ;
					state <= d2 ;
					done <= '0';
				elsif ( buf1(31 downto 28) = "1000" ) then
					reg_addressY <= buf1(27 downto 23);
					reg_addressX <= buf1(22 downto 18);
					reg_out <= '0' ;
					state <= d2 ;
					done <= '0';
				else
			--		reg_Y <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
					reg_out <= '0';
					state <= d3;
					done <= '0';
				end if;
			else
				if state = d2 then
					reg_out <= '1';
					if check = '1' then
						check <= '0';
						reg_Y <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
					end if;
					state <= d3;
				end if;
				if (state = d3) then
					--reg_out <= 'Z' ;
					if (buf1(31 downto 28) = "0011") or (buf1(31 downto 28) = "0010") then
						reg_Y <= "00000000000000" & buf1(22 downto 5);
						check <= '1';
					elsif  (buf1(31 downto 28) = "0000") or (buf1(31 downto 28) = "0001") or (buf1(31 downto 28) = "0100") or (buf1(31 downto 28) = "0101") or (buf1(31 downto 28) = "0110") or (buf1(31 downto 28) = "1000") then
					else	
						reg_Y <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
					end if;
					reg_addressY <= "ZZZZZ";
					reg_addressX <= "ZZZZZ";
					done <= '1' ;
					state <= d1 ;
					buf2 <= buf1;
				end if ;
			end if;
		end if;
		if rising_edge(clk) then
			
		end if;
	end process;
end decodebeh;
			
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;			
			
entity execute is
	Port(
		clk : in std_logic ;
		choice : out std_logic_vector(3 downto 0);
		start : in std_logic ;
		done : out std_logic ;
		buf2 : in std_logic_vector(31 downto 0);
		buf3 : out std_logic_vector(31 downto 0);
		pc : in std_logic_vector(7 downto 0);
		pcexecute : out std_logic_vector(7 downto 0);
		alu_out : in std_logic_vector(31 downto 0);
		jump : out std_logic
		);
end execute;

architecture executebeh of execute is
type states is (e1, e2, e3, e4);
signal state : states := e1;
attribute keep : string;
attribute keep of state : signal is "true";
begin
	process(clk) begin
		if rising_edge(clk) then
			if (start = '1') and (state = e1) then
				jump <= '0';
				if (buf2(31 downto 28) = "0000") or (buf2(31 downto 28) = "0010") or (buf2(31 downto 28) = "0011") then
					done <= '0';
					choice <= "1000";
					state <= e2;
				elsif (buf2(31 downto 28) = "0001") then
					done <= '0';
					choice <= "0001";
					state <= e2;
				elsif (buf2(31 downto 28) = "0100") then
					done <= '0';
					choice <= "0010";
					state <= e2;
				elsif (buf2(31 downto 28) = "0101") then
					done <= '0';
					choice <= "0011";
					state <= e2;
				elsif (buf2(31 downto 28) = "0110") then
					done <= '0';
					choice <= "0100";
					state <= e2;
				elsif (buf2(31 downto 28) = "1001") then
					pcexecute <= buf2(7 downto 0) ;
					done <= '0';
					state <= e2;
				elsif (buf2(31 downto 28) = "1000") then
					done <= '0';
					choice <= "0101";
					state <= e2;
				else
					buf3 <= buf2;
					done <= '1';
				end if;
			elsif (state = e2) then
				choice <= "1111";
				state <= e3;
			elsif (state = e3) then
				if (buf2(31 downto 28) = "1001") then
					done <= '1';
					jump <= '1';
					buf3 <= buf2;
					state <= e1;
				elsif (buf2(31 downto 28) = "1000") then
					state <= e4;
				elsif (buf2(31 downto 28) = "0000") or (buf2(31 downto 28) = "0001") or (buf2(31 downto 28) = "0100") or (buf2(31 downto 28) = "0101") or (buf2(31 downto 28) = "0110") or (buf2(31 downto 28) = "0011") or (buf2(31 downto 28) = "0010") then
					done <= '1';
					buf3 <= buf2;
					state <= e1;
				end if;
				choice <= "ZZZZ";
			elsif (state = e4) then
				if (buf2(31 downto 28) = "1000") then
					if (alu_out = "00000000000000000000000000000001") then
						pcexecute <= std_logic_vector(unsigned(pc)+ unsigned(buf2(7 downto 0)) - 2) ;
						done <= '1';
						jump <= '1';
						state <= e1;
						buf3 <= buf2;
					else
						done <= '1';
						state <= e1;
						buf3 <= buf2;
					end if;
				end if;
			end if;
		end if;
	end process;
end executebeh;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity store is
	Port(
			clk : in std_logic;
			start : in std_logic;
			done : out std_logic;
			buf3 : in std_logic_vector(31 downto 0);
			buf4 : out std_logic_vector(31 downto 0);
			mem_data : out std_logic_vector(31 downto 0);
			ram_address : out std_logic_vector(7 downto 0);
			ram_data_to : out std_logic_vector(31 downto 0) ;
			ram_data_from : in std_logic_vector(31 downto 0) ;
			ram_read : inout std_logic := 'Z';
			ram_write : out std_logic;
			reg_address : out std_logic_vector(4 downto 0);
			reg_in : out std_logic;
			reg_input_data : out std_logic_vector(31 downto 0) ;
			reg_to_mem_address : out std_logic_vector(4 downto 0) ;
			output_to_mem : in std_logic_vector(31 downto 0);
			r_mem : out std_logic ;
			reg_z : in std_logic_vector(31 downto 0)
		);
end store;

architecture storebeh of store is
type states is (s1, s2, s3, s4, sl1);
signal state : states := s1 ;
signal v :std_logic := '0';
attribute keep : string;
attribute keep of state : signal is "true";
begin
	process (clk) begin
		if rising_edge(clk) then
			if (start = '1') then
				if (buf3(31 downto 28) = "0010") then
					state <= sl1;
					done <= '0';
				elsif (buf3(31 downto 28) = "0011") then
					state <= s2;
					report "stage 3";
					done <= '0';
					reg_to_mem_address <= buf3(27 downto 23);
					r_mem <= '1';
				elsif (buf3(31 downto 28) = "0000") or (buf3(31 downto 28) = "0001") or (buf3(31 downto 28) = "0100") or (buf3(31 downto 28) = "0101") or (buf3(31 downto 28) = "0110") then
					state <= s2;
					if state = s2 then
						report "s2" ;
					else 
						report "Some other ." ;
					end if ;
					done <= '0';
					reg_address <= buf3(27 downto 23);
					reg_input_data <= reg_z;
					reg_in <= '1';
					v <='1';
					report "stage 1 vamsi is here";
				else
					report "YOU'RE SCREWED in STOREBEH!!" ;
					buf4 <= buf3;
					done <= '1';
				end if;
			else
				if (state = sl1) and (ram_read /= '1') then
					report "stage 2 for gods sake----chech state in store";
					state <= s2;
					ram_address <= reg_z(7 downto 0);
					ram_read <= '1';
				end if;
				if (state = s2) then
					if (buf3(31 downto 28) = "0000") or (buf3(31 downto 28) = "0001") or (buf3(31 downto 28) = "0100") or (buf3(31 downto 28) = "0101") or (buf3(31 downto 28) = "0110") then
						done <= '1';
						reg_in <= 'Z';
						reg_address <= "ZZZZZ";
						reg_input_data <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
						state <= s1;
						buf4 <= buf3;
					elsif (buf3(31 downto 28) = "0010") then
						state <=s3;
					elsif (buf3(31 downto 28) = "0011") then
						state <= s3;
					end if;
				elsif (state = s3) then
					if (buf3(31 downto 28) = "0010") then
						mem_data <= ram_data_from;
						ram_read <= 'Z';
						ram_address <= "ZZZZZZZZ";
						report "for fucks sake it cant be here" ;
						done <= '1';
						state <= s1;
						buf4 <= buf3;
					elsif (buf3(31 downto 28) = "0011") then
						ram_address <= reg_z(7 downto 0);
						r_mem <= 'Z';
						ram_data_to <= output_to_mem;
						ram_write <= '1';
						state <= s4;
					end if;
				elsif (state <= s4) then
					ram_write <= 'Z';
					ram_address <= "ZZZZZZZZ";
					done <= '1';
					buf4 <= buf3;
					state <= s1;
				end if;
			end if;
		end if;
	end process;
end storebeh;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity write_back is
	Port(
		buf4 : in std_logic_vector(31 downto 0);
		clk : in std_logic;
		start : in std_logic;
		done : out std_logic;
		write_address : out std_logic_vector(4 downto 0):="ZZZZZ";
		write_data : out std_logic_vector(31 downto 0):="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
		reg_write : inout std_logic:='Z';
		mem_data : in std_logic_vector(31 downto 0)
	);
end write_back;

architecture write_backbeh of write_back is
type states is (w1, w2, wl1);
signal state : states := w1;
attribute keep : string;
attribute keep of state : signal is "true";
begin
	process (clk) begin
		if rising_edge(clk) then 
			if (start = '1') then
				if (buf4(31 downto 28) = "0010") then
					state <= wl1;
					done <= '0';
				else
					done <= '1';
				end if;
			elsif (state = wl1) and (reg_write /= '1') then
				write_address <= buf4(27 downto 23);
				write_data <= mem_data;
				reg_write <= '1';
				state <= w2;
			elsif (state = w2) then
				done <= '1';
				state <= w1;
				write_address <= "ZZZZZ";
				write_data <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
				reg_write <= 'Z';
			end if;
		end if;
	end process;
end write_backbeh;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity controller is
	Port(
		clk : in std_logic ;
		regY : out std_logic_vector(31 downto 0) ;
		regZ : in std_logic_vector(31 downto 0) ;
		alu_choice : out std_logic_vector(3 downto 0) ;
		start : in std_logic ;
		done : out std_logic;
		r_address : out std_logic_vector(7 downto 0) ;
		r_read, r_write : inout std_logic ;
		reg_addressX : out std_logic_vector(4 downto 0) ;
		reg_addressY : out std_logic_vector(4 downto 0) ;
		reg_to_mem_address : out std_logic_vector(4 downto 0) ;
		reg_input_data : out std_logic_vector(31 downto 0) ;
		output_to_mem : in std_logic_vector(31 downto 0) ;
		reg_address : out std_logic_vector(4 downto 0) ;
		ram_data_to : out std_logic_vector(31 downto 0) ;
		ram_data_from : in std_logic_vector(31 downto 0) ;
		reg_in : inout std_logic ;
		reg_out , r_mem : out std_logic 
	) ;
end controller;
architecture controllerbeh of controller is
component fetch is
	Port(
		clk : in std_logic;
		start : in std_logic ;
		done : out std_logic ;
		pc : in std_logic_vector(7 downto 0) ;
		pcfetch : out std_logic_vector(7 downto 0) ;
		ram_address : out std_logic_vector(7 downto 0);
		buf1 : out std_logic_vector(31 downto 0) ;
		ram_data : in std_logic_vector(31 downto 0) ;
		ram_read : out std_logic
	) ;
	end component;
component decode is
	Port(
		clk : in std_logic ;
		start : in std_logic;
		done : out std_logic;
		buf1 : in std_logic_vector(31 downto 0);
		buf2 : out std_logic_vector(31 downto 0);
		reg_out : out std_logic ;
		reg_addressX : out std_logic_vector(4 downto 0);
		reg_Y : out std_logic_vector(31 downto 0);
		reg_addressY : out std_logic_vector(4 downto 0)
		) ;
	end component;
			
component execute is
	Port(
		clk : in std_logic ;
		choice : out std_logic_vector(3 downto 0);
		start : in std_logic ;
		done : out std_logic ;
		buf2 : in std_logic_vector(31 downto 0);
		buf3 : out std_logic_vector(31 downto 0);
		pc : in std_logic_vector(7 downto 0);
		pcexecute : out std_logic_vector(7 downto 0);
		alu_out : in std_logic_vector(31 downto 0);
		jump : out std_logic
		);
end component;

component store is
	Port(
			clk : in std_logic;
			start : in std_logic;
			done : out std_logic;
			buf3 : in std_logic_vector(31 downto 0);
			buf4 : out std_logic_vector(31 downto 0);
			mem_data : out std_logic_vector(31 downto 0);
			ram_address : out std_logic_vector(7 downto 0);
			ram_data_to : out std_logic_vector(31 downto 0) ;
			ram_data_from : in std_logic_vector(31 downto 0) ;
			ram_read : inout std_logic;
			ram_write : out std_logic;
			reg_address : out std_logic_vector(4 downto 0);
			reg_in : out std_logic;
			reg_input_data : out std_logic_vector(31 downto 0) ;
			reg_to_mem_address : out std_logic_vector(4 downto 0) ;
			output_to_mem : in std_logic_vector(31 downto 0);
			r_mem : out std_logic ;
			reg_z : in std_logic_vector(31 downto 0)
		);
end component;


component write_back is
	Port(
		buf4 : in std_logic_vector(31 downto 0);
		clk : in std_logic;
		start : in std_logic;
		done : out std_logic;
		write_address : out std_logic_vector(4 downto 0);
		write_data : out std_logic_vector(31 downto 0);
		reg_write : inout std_logic;
		mem_data : in std_logic_vector(31 downto 0)
	);
end component;
signal buf11 : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf11actual : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf22 : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf22actual : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf33 : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf33actual : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf44 : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal buf44actual : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal mem_data : std_logic_vector(31 downto 0) := "11111111111111111111111111111111";
signal start1 : std_logic := '0';
signal start2 : std_logic := '0';
signal start3 : std_logic := '0';
signal start4 : std_logic := '0';
signal start5 : std_logic := '0';
signal done1 : std_logic := '0';
signal done2 : std_logic := '0';
signal done3 : std_logic := '0';
signal done4 : std_logic := '0';
signal done5 : std_logic := '0';
signal pc : std_logic_vector(7 downto 0) := "00000000";
signal pcfetch : std_logic_vector(7 downto 0);
signal pcexecute : std_logic_vector(7 downto 0);
signal jump : std_logic := '0';
type states is ( s1,s2, s3, s4);
signal state : states := s1;

attribute keep : string;
attribute keep of state : signal is "true";
attribute keep of buf11 : signal is "true";
attribute keep of buf11actual : signal is "true";
attribute keep of buf22 : signal is "true";
attribute keep of buf22actual : signal is "true";
attribute keep of buf33 : signal is "true";
attribute keep of buf33actual : signal is "true";
attribute keep of buf44 : signal is "true";
attribute keep of buf44actual : signal is "true";
attribute keep of mem_data : signal is "true";
attribute keep of start1 : signal is "true";
attribute keep of start2 : signal is "true";
attribute keep of start3 : signal is "true";
attribute keep of start4 : signal is "true";
attribute keep of start5 : signal is "true";
attribute keep of done1 : signal is "true";
attribute keep of done2 : signal is "true";
attribute keep of done3 : signal is "true";
attribute keep of done4 : signal is "true";
attribute keep of done5 : signal is "true";
attribute keep of pc : signal is "true";
attribute keep of pcfetch : signal is "true";
attribute keep of pcexecute : signal is "true";
attribute keep of jump : signal is "true";

begin
	fetch1: fetch port map(clk , start1 ,done1 , pc, pcfetch , r_address , buf11 , ram_data_from ,  r_read );
	decode1 :decode port map(clk, start2, done2 , buf11actual , buf22 , reg_out ,reg_addressX , regY , reg_addressY );
	execute1: execute port map(clk , alu_choice , start3 , done3 , buf22actual , buf33 , pc, pcexecute , regZ, jump );
	store1: store port map(clk , start4 , done4 , buf33actual , buf44 , mem_data, r_address, ram_data_to, ram_data_from	, r_read , r_write , reg_address , reg_in , reg_input_data , reg_to_mem_address,output_to_mem, r_mem, regZ);
	write_back1 : write_back port map(buf44actual, clk, start5, done5, reg_address, reg_input_data, reg_in, mem_data);   
	process(clk,start, done1,done2,done3,done4,done5) begin	
		if rising_edge(clk) then
			if (start = '1') and (state = s1) then
				start1 <= '1';
				start2 <= '1';
				start3 <= '1';
				start4 <= '1';
				start5 <= '1';
				state <= s2;
			end if;
			if state = s2 then
			start1 <= '0';
			start2 <= '0';
			start3 <= '0';
			start4 <= '0';
			start5 <= '0';
			state <= s3;
			end if;
			if (done1 = '1') and (done2 = '1') and (done3 = '1') and (done4 = '1') and (done5 = '1') and state = s3 then
				state <= s4;
				buf33actual <= buf33;
				buf44actual <= buf44;
				if jump = '1' then
					buf11actual <= "11111111111111111111111111111111";
					buf22actual <= "11111111111111111111111111111111";
					pc <= pcexecute;
				else
					pc <= pcfetch;
					buf11actual <= buf11;
					buf22actual <= buf22;
				end if;
				if (buf11 = x"ffffffff") and (buf22 = x"ffffffff") and (buf33 = x"ffffffff") and (buf44 = x"ffffffff") then
					done <= '1';
				end if;
			end if;
			if state = s4 then
				state <= s1;
			end if;
		end if;
	end process;
end controllerbeh;