/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/thirteen13/Desktop/sulli - Copy/regs.vhd";
extern char *IEEE_P_1242562249;

int ieee_p_1242562249_sub_1657552908_1035706684(char *, char *, char *);


static void work_a_1111616105_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    int t7;
    int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(34, ng0);
    t1 = (t0 + 1052U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 2840);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 960U);
    t5 = *((char **)t1);
    t1 = (t0 + 868U);
    t6 = *((char **)t1);
    t1 = (t0 + 5128U);
    t7 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t6, t1);
    t8 = (t7 - 9);
    t9 = (t8 * -1);
    t10 = (32U * t9);
    t11 = (0U + t10);
    t12 = (t0 + 2900);
    t13 = (t12 + 32U);
    t14 = *((char **)t13);
    t15 = (t14 + 40U);
    t16 = *((char **)t15);
    memcpy(t16, t5, 32U);
    xsi_driver_first_trans_delta(t12, t11, 32U, 0LL);
    goto LAB3;

}

static void work_a_1111616105_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    int t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;

LAB0:    xsi_set_current_line(40, ng0);
    t1 = (t0 + 1144U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(48, ng0);
    t1 = (t0 + 5714);
    t5 = (t0 + 2972);
    t6 = (t5 + 32U);
    t9 = *((char **)t6);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    memcpy(t11, t1, 32U);
    xsi_driver_first_trans_fast_port(t5);

LAB3:    t1 = (t0 + 2848);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(41, ng0);
    t1 = (t0 + 684U);
    t5 = *((char **)t1);
    t1 = (t0 + 5704);
    t7 = 1;
    if (5U == 5U)
        goto LAB8;

LAB9:    t7 = 0;

LAB10:    if ((!(t7)) != 0)
        goto LAB5;

LAB7:
LAB6:    xsi_set_current_line(44, ng0);
    t1 = (t0 + 776U);
    t2 = *((char **)t1);
    t1 = (t0 + 5709);
    t3 = 1;
    if (5U == 5U)
        goto LAB17;

LAB18:    t3 = 0;

LAB19:    if ((!(t3)) != 0)
        goto LAB14;

LAB16:
LAB15:    goto LAB3;

LAB5:    xsi_set_current_line(42, ng0);
    t11 = (t0 + 1696U);
    t12 = *((char **)t11);
    t11 = (t0 + 684U);
    t13 = *((char **)t11);
    t11 = (t0 + 5096U);
    t14 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t13, t11);
    t15 = (t14 - 9);
    t16 = (t15 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t14);
    t17 = (32U * t16);
    t18 = (0 + t17);
    t19 = (t12 + t18);
    t20 = (t0 + 2936);
    t21 = (t20 + 32U);
    t22 = *((char **)t21);
    t23 = (t22 + 40U);
    t24 = *((char **)t23);
    memcpy(t24, t19, 32U);
    xsi_driver_first_trans_fast_port(t20);
    goto LAB6;

LAB8:    t8 = 0;

LAB11:    if (t8 < 5U)
        goto LAB12;
    else
        goto LAB10;

LAB12:    t9 = (t5 + t8);
    t10 = (t1 + t8);
    if (*((unsigned char *)t9) != *((unsigned char *)t10))
        goto LAB9;

LAB13:    t8 = (t8 + 1);
    goto LAB11;

LAB14:    xsi_set_current_line(45, ng0);
    t10 = (t0 + 1696U);
    t11 = *((char **)t10);
    t10 = (t0 + 776U);
    t12 = *((char **)t10);
    t10 = (t0 + 5112U);
    t14 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t12, t10);
    t15 = (t14 - 9);
    t16 = (t15 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t14);
    t17 = (32U * t16);
    t18 = (0 + t17);
    t13 = (t11 + t18);
    t19 = (t0 + 2972);
    t20 = (t19 + 32U);
    t21 = *((char **)t20);
    t22 = (t21 + 40U);
    t23 = *((char **)t22);
    memcpy(t23, t13, 32U);
    xsi_driver_first_trans_fast_port(t19);
    goto LAB15;

LAB17:    t8 = 0;

LAB20:    if (t8 < 5U)
        goto LAB21;
    else
        goto LAB19;

LAB21:    t6 = (t2 + t8);
    t9 = (t1 + t8);
    if (*((unsigned char *)t6) != *((unsigned char *)t9))
        goto LAB18;

LAB22:    t8 = (t8 + 1);
    goto LAB20;

}

static void work_a_1111616105_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    int t7;
    int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;

LAB0:    xsi_set_current_line(52, ng0);
    t1 = (t0 + 1236U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 2856);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 1696U);
    t5 = *((char **)t1);
    t1 = (t0 + 1604U);
    t6 = *((char **)t1);
    t1 = (t0 + 5208U);
    t7 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t6, t1);
    t8 = (t7 - 9);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t7);
    t10 = (32U * t9);
    t11 = (0 + t10);
    t12 = (t5 + t11);
    t13 = (t0 + 3008);
    t14 = (t13 + 32U);
    t15 = *((char **)t14);
    t16 = (t15 + 40U);
    t17 = *((char **)t16);
    memcpy(t17, t12, 32U);
    xsi_driver_first_trans_fast_port(t13);
    goto LAB3;

}


extern void work_a_1111616105_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1111616105_3212880686_p_0,(void *)work_a_1111616105_3212880686_p_1,(void *)work_a_1111616105_3212880686_p_2};
	xsi_register_didat("work_a_1111616105_3212880686", "isim/top_module_isim_beh.exe.sim/work/a_1111616105_3212880686.didat");
	xsi_register_executes(pe);
}
