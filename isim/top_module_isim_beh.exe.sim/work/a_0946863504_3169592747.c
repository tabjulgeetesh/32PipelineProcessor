/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/thirteen13/Desktop/sulli - Copy/contr.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_1242562249;

int ieee_p_1242562249_sub_1657552908_1035706684(char *, char *, char *);
char *ieee_p_1242562249_sub_180853171_1035706684(char *, char *, int , int );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_0946863504_3169592747_p_0(char *t0)
{
    char t19[16];
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    int t20;
    int t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(27, ng0);
    t2 = (t0 + 568U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 568U);
    t1 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t1 != 0)
        goto LAB11;

LAB13:
LAB12:    t2 = (t0 + 2276);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(28, ng0);
    t4 = (t0 + 1420U);
    t8 = *((char **)t4);
    t9 = *((unsigned char *)t8);
    t10 = (t9 == (unsigned char)0);
    if (t10 != 0)
        goto LAB8;

LAB10:
LAB9:    goto LAB3;

LAB5:    t4 = (t0 + 684U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB7;

LAB8:    xsi_set_current_line(29, ng0);
    t4 = (t0 + 2320);
    t11 = (t4 + 32U);
    t12 = *((char **)t11);
    t13 = (t12 + 40U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(30, ng0);
    t2 = (t0 + 4311);
    xsi_report(t2, 12U, 0);
    xsi_set_current_line(31, ng0);
    t2 = (t0 + 868U);
    t4 = *((char **)t2);
    t2 = (t0 + 2356);
    t5 = (t2 + 32U);
    t8 = *((char **)t5);
    t11 = (t8 + 40U);
    t12 = *((char **)t11);
    memcpy(t12, t4, 8U);
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(32, ng0);
    t2 = (t0 + 2392);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t8 = (t5 + 40U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(33, ng0);
    t2 = (t0 + 2428);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t8 = (t5 + 40U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = (unsigned char)1;
    xsi_driver_first_trans_fast(t2);
    goto LAB9;

LAB11:    xsi_set_current_line(37, ng0);
    t4 = (t0 + 1420U);
    t5 = *((char **)t4);
    t3 = *((unsigned char *)t5);
    t6 = (t3 == (unsigned char)2);
    if (t6 != 0)
        goto LAB14;

LAB16:    t2 = (t0 + 1420U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)1);
    if (t3 != 0)
        goto LAB26;

LAB27:
LAB15:    goto LAB12;

LAB14:    xsi_set_current_line(38, ng0);
    t4 = (t0 + 1236U);
    t8 = *((char **)t4);
    t4 = (t0 + 2464);
    t11 = (t4 + 32U);
    t12 = *((char **)t11);
    t13 = (t12 + 40U);
    t14 = *((char **)t13);
    memcpy(t14, t8, 32U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(39, ng0);
    t2 = (t0 + 2392);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t8 = (t5 + 40U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = (unsigned char)4;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(40, ng0);
    t2 = (t0 + 4323);
    t5 = (t0 + 2356);
    t8 = (t5 + 32U);
    t11 = *((char **)t8);
    t12 = (t11 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 8U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(41, ng0);
    t2 = (t0 + 1236U);
    t4 = *((char **)t2);
    t15 = (31 - 31);
    t16 = (t15 * 1U);
    t17 = (0 + t16);
    t2 = (t4 + t17);
    t5 = (t0 + 4331);
    t1 = 1;
    if (4U == 4U)
        goto LAB20;

LAB21:    t1 = 0;

LAB22:    if ((!(t1)) != 0)
        goto LAB17;

LAB19:    xsi_set_current_line(44, ng0);
    t2 = (t0 + 868U);
    t4 = *((char **)t2);
    t2 = (t0 + 2500);
    t5 = (t2 + 32U);
    t8 = *((char **)t5);
    t11 = (t8 + 40U);
    t12 = *((char **)t11);
    memcpy(t12, t4, 8U);
    xsi_driver_first_trans_fast_port(t2);

LAB18:    xsi_set_current_line(46, ng0);
    t2 = (t0 + 2320);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t8 = (t5 + 40U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(47, ng0);
    t2 = (t0 + 2428);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t8 = (t5 + 40U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = (unsigned char)0;
    xsi_driver_first_trans_fast(t2);
    goto LAB15;

LAB17:    xsi_set_current_line(42, ng0);
    t13 = (t0 + 868U);
    t14 = *((char **)t13);
    t13 = (t0 + 4180U);
    t20 = ieee_p_1242562249_sub_1657552908_1035706684(IEEE_P_1242562249, t14, t13);
    t21 = (t20 + 1);
    t22 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t19, t21, 8);
    t23 = (t0 + 2500);
    t24 = (t23 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t22, 8U);
    xsi_driver_first_trans_fast_port(t23);
    goto LAB18;

LAB20:    t18 = 0;

LAB23:    if (t18 < 4U)
        goto LAB24;
    else
        goto LAB22;

LAB24:    t11 = (t2 + t18);
    t12 = (t5 + t18);
    if (*((unsigned char *)t11) != *((unsigned char *)t12))
        goto LAB21;

LAB25:    t18 = (t18 + 1);
    goto LAB23;

LAB26:    xsi_set_current_line(49, ng0);
    t2 = (t0 + 2428);
    t5 = (t2 + 32U);
    t8 = *((char **)t5);
    t11 = (t8 + 40U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    goto LAB15;

}


extern void work_a_0946863504_3169592747_init()
{
	static char *pe[] = {(void *)work_a_0946863504_3169592747_p_0};
	xsi_register_didat("work_a_0946863504_3169592747", "isim/top_module_isim_beh.exe.sim/work/a_0946863504_3169592747.didat");
	xsi_register_executes(pe);
}
