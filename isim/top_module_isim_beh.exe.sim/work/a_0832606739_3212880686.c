/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/thirteen13/Desktop/sulli - Copy/alu1.vhd";
extern char *IEEE_P_1242562249;
extern char *IEEE_P_2592010699;

char *ieee_p_1242562249_sub_1547198987_1035706684(char *, char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_1547270861_1035706684(char *, char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_2110375371_1035706684(char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_2110411308_1035706684(char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_1735675855_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_795620321_503743352(char *, char *, char *, char *, char *, char *);


static void work_a_0832606739_3212880686_p_0(char *t0)
{
    char t8[16];
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    char *t22;

LAB0:    xsi_set_current_line(22, ng0);
    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3542);
    t4 = 1;
    if (4U == 4U)
        goto LAB5;

LAB6:    t4 = 0;

LAB7:    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3562);
    t4 = 1;
    if (4U == 4U)
        goto LAB13;

LAB14:    t4 = 0;

LAB15:    if (t4 != 0)
        goto LAB11;

LAB12:    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3585);
    t4 = 1;
    if (4U == 4U)
        goto LAB21;

LAB22:    t4 = 0;

LAB23:    if (t4 != 0)
        goto LAB19;

LAB20:    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3589);
    t4 = 1;
    if (4U == 4U)
        goto LAB31;

LAB32:    t4 = 0;

LAB33:    if (t4 != 0)
        goto LAB29;

LAB30:    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3593);
    t4 = 1;
    if (4U == 4U)
        goto LAB41;

LAB42:    t4 = 0;

LAB43:    if (t4 != 0)
        goto LAB39;

LAB40:    t1 = (t0 + 960U);
    t2 = *((char **)t1);
    t1 = (t0 + 3661);
    t4 = 1;
    if (4U == 4U)
        goto LAB52;

LAB53:    t4 = 0;

LAB54:    if (t4 != 0)
        goto LAB50;

LAB51:
LAB3:    t1 = (t0 + 1908);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(23, ng0);
    t9 = (t0 + 776U);
    t10 = *((char **)t9);
    t9 = (t0 + 3384U);
    t11 = (t0 + 684U);
    t12 = *((char **)t11);
    t11 = (t0 + 3368U);
    t13 = ieee_p_1242562249_sub_1547198987_1035706684(IEEE_P_1242562249, t8, t10, t9, t12, t11);
    t14 = (t0 + 1952);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    memcpy(t18, t13, 32U);
    xsi_driver_first_trans_fast_port(t14);
    xsi_set_current_line(24, ng0);
    t1 = (t0 + 3546);
    xsi_report(t1, 16U, 0);
    goto LAB3;

LAB5:    t5 = 0;

LAB8:    if (t5 < 4U)
        goto LAB9;
    else
        goto LAB7;

LAB9:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB6;

LAB10:    t5 = (t5 + 1);
    goto LAB8;

LAB11:    xsi_set_current_line(27, ng0);
    t9 = (t0 + 776U);
    t10 = *((char **)t9);
    t9 = (t0 + 3384U);
    t11 = (t0 + 684U);
    t12 = *((char **)t11);
    t11 = (t0 + 3368U);
    t13 = ieee_p_1242562249_sub_1547270861_1035706684(IEEE_P_1242562249, t8, t10, t9, t12, t11);
    t14 = (t0 + 1952);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    memcpy(t18, t13, 32U);
    xsi_driver_first_trans_fast_port(t14);
    xsi_set_current_line(28, ng0);
    t1 = (t0 + 3566);
    xsi_report(t1, 19U, 0);
    goto LAB3;

LAB13:    t5 = 0;

LAB16:    if (t5 < 4U)
        goto LAB17;
    else
        goto LAB15;

LAB17:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB14;

LAB18:    t5 = (t5 + 1);
    goto LAB16;

LAB19:    xsi_set_current_line(31, ng0);
    t9 = (t0 + 776U);
    t10 = *((char **)t9);
    t9 = (t0 + 3384U);
    t11 = (t0 + 684U);
    t12 = *((char **)t11);
    t11 = (t0 + 3368U);
    t13 = ieee_p_2592010699_sub_795620321_503743352(IEEE_P_2592010699, t8, t10, t9, t12, t11);
    t14 = (t8 + 12U);
    t19 = *((unsigned int *)t14);
    t20 = (1U * t19);
    t21 = (32U != t20);
    if (t21 == 1)
        goto LAB27;

LAB28:    t15 = (t0 + 1952);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    t18 = (t17 + 40U);
    t22 = *((char **)t18);
    memcpy(t22, t13, 32U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB3;

LAB21:    t5 = 0;

LAB24:    if (t5 < 4U)
        goto LAB25;
    else
        goto LAB23;

LAB25:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB22;

LAB26:    t5 = (t5 + 1);
    goto LAB24;

LAB27:    xsi_size_not_matching(32U, t20, 0);
    goto LAB28;

LAB29:    xsi_set_current_line(34, ng0);
    t9 = (t0 + 776U);
    t10 = *((char **)t9);
    t9 = (t0 + 3384U);
    t11 = (t0 + 684U);
    t12 = *((char **)t11);
    t11 = (t0 + 3368U);
    t13 = ieee_p_2592010699_sub_1735675855_503743352(IEEE_P_2592010699, t8, t10, t9, t12, t11);
    t14 = (t8 + 12U);
    t19 = *((unsigned int *)t14);
    t20 = (1U * t19);
    t21 = (32U != t20);
    if (t21 == 1)
        goto LAB37;

LAB38:    t15 = (t0 + 1952);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    t18 = (t17 + 40U);
    t22 = *((char **)t18);
    memcpy(t22, t13, 32U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB3;

LAB31:    t5 = 0;

LAB34:    if (t5 < 4U)
        goto LAB35;
    else
        goto LAB33;

LAB35:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB32;

LAB36:    t5 = (t5 + 1);
    goto LAB34;

LAB37:    xsi_size_not_matching(32U, t20, 0);
    goto LAB38;

LAB39:    xsi_set_current_line(37, ng0);
    t9 = (t0 + 684U);
    t10 = *((char **)t9);
    t9 = (t0 + 3368U);
    t11 = (t0 + 776U);
    t12 = *((char **)t11);
    t11 = (t0 + 3384U);
    t21 = ieee_p_1242562249_sub_2110411308_1035706684(IEEE_P_1242562249, t10, t9, t12, t11);
    if (t21 != 0)
        goto LAB47;

LAB49:    xsi_set_current_line(40, ng0);
    t1 = (t0 + 3629);
    t3 = (t0 + 1952);
    t6 = (t3 + 32U);
    t7 = *((char **)t6);
    t9 = (t7 + 40U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 32U);
    xsi_driver_first_trans_fast_port(t3);

LAB48:    goto LAB3;

LAB41:    t5 = 0;

LAB44:    if (t5 < 4U)
        goto LAB45;
    else
        goto LAB43;

LAB45:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB42;

LAB46:    t5 = (t5 + 1);
    goto LAB44;

LAB47:    xsi_set_current_line(38, ng0);
    t13 = (t0 + 3597);
    t15 = (t0 + 1952);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    t18 = (t17 + 40U);
    t22 = *((char **)t18);
    memcpy(t22, t13, 32U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB48;

LAB50:    xsi_set_current_line(44, ng0);
    t9 = (t0 + 776U);
    t10 = *((char **)t9);
    t9 = (t0 + 3384U);
    t11 = (t0 + 684U);
    t12 = *((char **)t11);
    t11 = (t0 + 3368U);
    t21 = ieee_p_1242562249_sub_2110375371_1035706684(IEEE_P_1242562249, t10, t9, t12, t11);
    if (t21 != 0)
        goto LAB58;

LAB60:    xsi_set_current_line(47, ng0);
    t1 = (t0 + 3697);
    t3 = (t0 + 1952);
    t6 = (t3 + 32U);
    t7 = *((char **)t6);
    t9 = (t7 + 40U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 32U);
    xsi_driver_first_trans_fast_port(t3);

LAB59:    goto LAB3;

LAB52:    t5 = 0;

LAB55:    if (t5 < 4U)
        goto LAB56;
    else
        goto LAB54;

LAB56:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB53;

LAB57:    t5 = (t5 + 1);
    goto LAB55;

LAB58:    xsi_set_current_line(45, ng0);
    t13 = (t0 + 3665);
    t15 = (t0 + 1952);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    t18 = (t17 + 40U);
    t22 = *((char **)t18);
    memcpy(t22, t13, 32U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB59;

}


extern void work_a_0832606739_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0832606739_3212880686_p_0};
	xsi_register_didat("work_a_0832606739_3212880686", "isim/top_module_isim_beh.exe.sim/work/a_0832606739_3212880686.didat");
	xsi_register_executes(pe);
}
