/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/thirteen13/Desktop/sulli - Copy/contr.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3200707899_4220991613_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned char t13;
    unsigned int t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    unsigned char t22;

LAB0:    xsi_set_current_line(375, ng0);
    t1 = (t0 + 660U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 2184);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(376, ng0);
    t3 = (t0 + 776U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    t1 = (t0 + 1328U);
    t3 = *((char **)t1);
    t5 = *((unsigned char *)t3);
    t6 = (t5 == (unsigned char)2);
    if (t6 == 1)
        goto LAB19;

LAB20:    t2 = (unsigned char)0;

LAB21:    if (t2 != 0)
        goto LAB17;

LAB18:    t1 = (t0 + 1328U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)1);
    if (t5 != 0)
        goto LAB22;

LAB23:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(377, ng0);
    t3 = (t0 + 592U);
    t7 = *((char **)t3);
    t8 = (31 - 31);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t3 = (t7 + t10);
    t11 = (t0 + 4181);
    t13 = 1;
    if (4U == 4U)
        goto LAB11;

LAB12:    t13 = 0;

LAB13:    if (t13 != 0)
        goto LAB8;

LAB10:    xsi_set_current_line(381, ng0);
    t1 = (t0 + 2264);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);

LAB9:    goto LAB6;

LAB8:    xsi_set_current_line(378, ng0);
    t17 = (t0 + 2228);
    t18 = (t17 + 32U);
    t19 = *((char **)t18);
    t20 = (t19 + 40U);
    t21 = *((char **)t20);
    *((unsigned char *)t21) = (unsigned char)2;
    xsi_driver_first_trans_fast(t17);
    xsi_set_current_line(379, ng0);
    t1 = (t0 + 2264);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB9;

LAB11:    t14 = 0;

LAB14:    if (t14 < 4U)
        goto LAB15;
    else
        goto LAB13;

LAB15:    t15 = (t3 + t14);
    t16 = (t11 + t14);
    if (*((unsigned char *)t15) != *((unsigned char *)t16))
        goto LAB12;

LAB16:    t14 = (t14 + 1);
    goto LAB14;

LAB17:    xsi_set_current_line(384, ng0);
    t1 = (t0 + 592U);
    t7 = *((char **)t1);
    t8 = (31 - 27);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t1 = (t7 + t10);
    t11 = (t0 + 2300);
    t12 = (t11 + 32U);
    t15 = *((char **)t12);
    t16 = (t15 + 40U);
    t17 = *((char **)t16);
    memcpy(t17, t1, 5U);
    xsi_driver_first_trans_fast_port(t11);
    xsi_set_current_line(385, ng0);
    t1 = (t0 + 1236U);
    t3 = *((char **)t1);
    t1 = (t0 + 2336);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t11 = (t7 + 40U);
    t12 = *((char **)t11);
    memcpy(t12, t3, 32U);
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(386, ng0);
    t1 = (t0 + 2372);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(387, ng0);
    t1 = (t0 + 2228);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);
    goto LAB6;

LAB19:    t1 = (t0 + 1144U);
    t4 = *((char **)t1);
    t13 = *((unsigned char *)t4);
    t22 = (t13 != (unsigned char)3);
    t2 = t22;
    goto LAB21;

LAB22:    xsi_set_current_line(389, ng0);
    t1 = (t0 + 2264);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t11 = (t7 + 40U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(390, ng0);
    t1 = (t0 + 2228);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(391, ng0);
    t1 = (t0 + 4185);
    t4 = (t0 + 2300);
    t7 = (t4 + 32U);
    t11 = *((char **)t7);
    t12 = (t11 + 40U);
    t15 = *((char **)t12);
    memcpy(t15, t1, 5U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(392, ng0);
    t1 = (t0 + 4190);
    t4 = (t0 + 2336);
    t7 = (t4 + 32U);
    t11 = *((char **)t7);
    t12 = (t11 + 40U);
    t15 = *((char **)t12);
    memcpy(t15, t1, 32U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(393, ng0);
    t1 = (t0 + 2372);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t11 = *((char **)t7);
    *((unsigned char *)t11) = (unsigned char)4;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB6;

}


extern void work_a_3200707899_4220991613_init()
{
	static char *pe[] = {(void *)work_a_3200707899_4220991613_p_0};
	xsi_register_didat("work_a_3200707899_4220991613", "isim/top_module_isim_beh.exe.sim/work/a_3200707899_4220991613.didat");
	xsi_register_executes(pe);
}
