library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top_module is
	Port(
		clk : in std_logic ;
		start : in std_logic ;
		final_output : out std_logic_vector (7 downto 0);
		output : out std_logic_vector(31 downto 0) 
	) ;
end top_module;
architecture Behavioral of top_module is
	component alu
		Port(
		clk : in std_logic ;
		regX : in std_logic_vector (31 downto 0);
		regY : in std_logic_vector(31 downto 0) ;
		regZ : out std_logic_vector(31 downto 0) := x"00000000";
		choice : in std_logic_vector(3 downto 0) 
	) ;
	end component ;
	component regFile
		Port(
		clk : in std_logic ;
		regXAddress : in std_logic_vector (4 downto 0);
		regYAddress : in std_logic_vector (4 downto 0);
		writeAddress : in std_logic_vector (4 downto 0);
		regInputData : in std_logic_vector(31 downto 0) ;
		rin : in std_logic ;
		rout : in std_logic ;
		r_mem : in std_logic ;
		output_to_mem : out std_logic_vector(31 downto 0);
		outputRegX : out std_logic_vector(31 downto 0) ;
		outputRegY : out std_logic_vector(31 downto 0);
		reg_to_mem_address : in std_logic_vector(4 downto 0) 
	) ;
	end component ;
	component ram
		Port(
			clk : in std_logic ;
			address : in std_logic_vector(7 downto 0) ;
			d_in : in std_logic_vector(31 downto 0) ;
			d_out : out std_logic_vector(31 downto 0) ;
			r_read : in std_logic ;
			r_write : in std_logic;
			done : in std_logic;
			final_output : out std_logic_vector(7 downto 0)
	) ;
	end component ;
	component controller
		Port(
		clk : in std_logic ;
	--	dbus_mem : inout std_logic_vector(31 downto 0) ; --input from RAM
		regY : out std_logic_vector(31 downto 0) ;
		regZ : in std_logic_vector(31 downto 0) ;
		alu_choice : out std_logic_vector(3 downto 0) ;
		start : in std_logic ;
		done : out std_logic;
		r_address : out std_logic_vector(7 downto 0) ;
		r_read, r_write : inout std_logic ;
		reg_addressX : out std_logic_vector(4 downto 0) ;
		reg_addressY : out std_logic_vector(4 downto 0) ;
		reg_to_mem_address : out std_logic_vector(4 downto 0) ;
		reg_input_data : out std_logic_vector(31 downto 0) ;
		output_to_mem : in std_logic_vector(31 downto 0) ;
		reg_address : out std_logic_vector(4 downto 0) ;
		ram_data_to : out std_logic_vector(31 downto 0) ;
		ram_data_from : in std_logic_vector(31 downto 0) ;
		reg_in : inout std_logic;
		reg_out , r_mem : out std_logic 
	) ;
	end component;
	signal regX : std_logic_vector(31 downto 0) := x"00000000";
	signal regY : std_logic_vector(31 downto 0) := x"00000000";
	signal regZ : std_logic_vector(31 downto 0) := x"00000000";
	signal alu_choice : std_logic_vector(3 downto 0) := "1111";
	signal regXAddress :  std_logic_vector(4 downto 0);
	signal regYAddress :  std_logic_vector (4 downto 0);
	signal writeAddress :  std_logic_vector (4 downto 0);
	signal regInputData :  std_logic_vector(31 downto 0) ;
	signal rin :  std_logic ;
	signal rout :  std_logic ;
	signal r_mem :  std_logic ;
	signal output_to_mem :  std_logic_vector(31 downto 0);
	signal reg_to_mem_address :  std_logic_vector(4 downto 0);
	signal ram_address :  std_logic_vector(7 downto 0) ;
	signal data_to_ram :  std_logic_vector(31 downto 0) ;
	signal data_from_ram :  std_logic_vector(31 downto 0) ;
	signal r_read :  std_logic ;
	signal r_write :  std_logic;
	signal done : std_logic := '0';
	
	attribute keep : string;
	attribute keep of regX : signal is "true";
	attribute keep of regY : signal is "true";
	attribute keep of regZ : signal is "true";
	attribute keep of alu_choice : signal is "true";
	attribute keep of regXAddress : signal is "true";
	attribute keep of regYAddress : signal is "true";
	attribute keep of writeAddress : signal is "true";
	attribute keep of regInputData : signal is "true";
	attribute keep of rin : signal is "true";
	attribute keep of rout : signal is "true";
	attribute keep of r_mem : signal is "true";
	attribute keep of output_to_mem : signal is "true";
	attribute keep of reg_to_mem_address : signal is "true";
	attribute keep of ram_address : signal is "true";
	attribute keep of data_to_ram : signal is "true";
	attribute keep of data_from_ram : signal is "true";
	attribute keep of r_read : signal is "true";
	attribute keep of r_write : signal is "true";
	attribute keep of start : signal is "true";
	attribute keep of done : signal is "true";
begin
	alu1 : alu port map(clk, regX , regY , regZ , alu_choice) ;
	regs : regFile port map(clk, regXAddress, regYAddress, writeAddress, regInputData, rin,rout,r_mem, output_to_mem, regX,regY, reg_to_mem_address) ;
	rams : ram port map(clk, ram_address, data_to_ram, data_from_ram, r_read, r_write,done,final_output) ;
	contr : controller port map(clk, regY, regZ, alu_choice, start, done, ram_address, r_read, r_write, regXAddress, regYAddress, reg_to_mem_address, regInputData, output_to_mem, writeAddress, data_to_ram, data_from_ram, rin, rout, r_mem) ;
 output <= regZ;

end Behavioral;