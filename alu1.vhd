library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity alu is
	Port(
		clk : in std_logic ;
		regX : in std_logic_vector (31 downto 0);
		regY : in std_logic_vector(31 downto 0) ;
		regZ : out std_logic_vector(31 downto 0) := x"00000000";
		choice : in std_logic_vector(3 downto 0) 
	) ;
end alu;

architecture Behavioral of alu is
type states is (first, second);
signal state : states := first;
begin
	process(choice)  begin
		--if (rising_edge(clk)) then
			--if (state = first) then
				if choice = "1000" then
					regZ <= std_logic_vector(unsigned(regY) + unsigned(regX)) ;
					Report "ran the addition";
					--state <= second;
				elsif choice = "0001" then
					regZ <= std_logic_vector(unsigned(regY) - unsigned(regX)) ;
					Report "ran the subtraction";
					--state <= second;
				elsif choice = "0010" then
					regZ <= regY and regX ;
				--	state <= second;
				elsif choice = "0011" then
					regZ <= regY or regX ;
			--		state <= second;
				elsif choice = "0100" then
					if unsigned(regX) > unsigned(regY) then
						regz <= "00000000000000000000000000000001" ;
					else
						regz <= x"00000000" ;
					end if ;
		--			state <= second;
				elsif choice = "0101" then
					if unsigned(regY) = unsigned(regX) then
						regz <= "00000000000000000000000000000001" ;
					else
						regz <= x"00000000" ;
					end if ;
			--		state <= second;
				else
				
				end if ;
			--elsif (state = second) then
				--state <= first ;
			--end if;
		--end if;
	end process ;

end Behavioral;